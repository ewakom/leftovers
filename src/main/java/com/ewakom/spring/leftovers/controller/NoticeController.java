package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.NoticeService;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateNoticeDTO;
import com.ewakom.spring.leftovers.service.dto.NoticeDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/notices")
public class NoticeController {
    @Autowired
    private NoticeService noticeService;

    @GetMapping
    private List<NoticeDTO> getAllNotices() {
        return noticeService.getAllNotices();
    }

    @GetMapping("/{noticeId}")
    private NoticeDTO getNoticeById(@PathVariable UUID noticeId) throws NotFound {
        return noticeService.getNoticeById(noticeId);
    }

    @PostMapping
    private NoticeDTO addNotice(@RequestBody CreateUpdateNoticeDTO newNotice) throws DataInvalid, NotFound {
        return noticeService.addNotice(newNotice);
    }

    @PutMapping("/{noticeId}")
    private NoticeDTO updateNotice(@PathVariable UUID noticeId, @RequestBody CreateUpdateNoticeDTO newNoticeData) throws DataInvalid, NotFound {
        return noticeService.updateNotice(noticeId, newNoticeData);
    }

    @DeleteMapping("/{noticeId}")
    private NoticeDTO deleteNoticeById(@PathVariable UUID noticeId) throws NotFound {
        return noticeService.deleteNoticeById(noticeId);
    }
}
