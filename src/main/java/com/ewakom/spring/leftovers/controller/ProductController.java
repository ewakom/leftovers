package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.ProductService;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateProductDTO;
import com.ewakom.spring.leftovers.service.dto.ProductDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping
    private List<ProductDTO> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/{productId}")
    private ProductDTO getProductById(@PathVariable UUID productId) throws NotFound {
        return productService.getProductById(productId);
    }

    @PostMapping
    private ProductDTO addProduct(@RequestBody CreateUpdateProductDTO newProduct) throws DataInvalid {
        return productService.addProduct(newProduct);
    }

    @PutMapping("/{productId}")
    private ProductDTO updateProduct(@PathVariable UUID productId, @RequestBody CreateUpdateProductDTO newProductDate) throws DataInvalid, NotFound {
        return productService.updateProduct(productId, newProductDate);
    }

    @DeleteMapping("/{productId}")
    private ProductDTO deleteProductById(@PathVariable UUID productId) throws NotFound {
        return productService.deleteProductById(productId);
    }
}
