package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.BunchService;
import com.ewakom.spring.leftovers.service.dto.BunchDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateBunchDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/bunches")
public class BunchController {
    @Autowired
    private BunchService bunchService;

    @GetMapping
    private List<BunchDTO> getAllBunches() {
        return bunchService.getAllBunches();
    }

    @GetMapping("/{bunchId}")
    private BunchDTO getBunchById(@PathVariable UUID bunchId) throws NotFound {
        return bunchService.getBunchById(bunchId);
    }

    @PostMapping
    private BunchDTO addBunch(@RequestBody CreateUpdateBunchDTO newBunch) throws DataInvalid {
        return bunchService.addBunch(newBunch);
    }

    @PutMapping("/{bunchId}")
    private BunchDTO updateBunch(@PathVariable UUID bunchId, @RequestBody CreateUpdateBunchDTO newBunchData) throws DataInvalid, NotFound {
        return bunchService.updateBunch(bunchId, newBunchData);
    }

    @DeleteMapping("/{bunchId}")
    private BunchDTO deleteBunchById(@PathVariable UUID bunchId) throws NotFound {
        return bunchService.deleteBunchById(bunchId);
    }
}
