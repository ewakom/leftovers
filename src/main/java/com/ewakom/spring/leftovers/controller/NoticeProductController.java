package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.NoticeProductService;
import com.ewakom.spring.leftovers.service.dto.NoticeDTO;
import com.ewakom.spring.leftovers.service.dto.ProductDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class NoticeProductController {
    @Autowired
    private NoticeProductService noticeProductService;

    @GetMapping("/notices/{noticeId}/products")
    private List<ProductDTO> getAllProductsForNotice(@PathVariable UUID noticeId) throws NotFound {
        return noticeProductService.getAllProductsForNotice(noticeId);
    }

    @PostMapping("/notices/{noticeId}/products/{productId}")
    private NoticeDTO addProductToNotice(@PathVariable UUID noticeId, @PathVariable UUID productId) throws NotFound {
        return noticeProductService.addProductToNotice(noticeId, productId);
    }

    @DeleteMapping("/notices/{noticeId}/products/{productId}")
    private NoticeDTO deleteProductFromNotice(@PathVariable UUID noticeId, @PathVariable UUID productId) throws NotFound {
        return noticeProductService.deleteProductFromNotice(noticeId, productId);
    }
}
