package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.CommentService;
import com.ewakom.spring.leftovers.service.dto.CommentDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateCommentDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping
    private List<CommentDTO> getAllComments() {
        return commentService.getAllComments();
    }

    @GetMapping("/{commentId}")
    private CommentDTO getCommentById(@PathVariable UUID commentId) throws NotFound {
        return commentService.getCommentById(commentId);
    }

    @PostMapping
    private CommentDTO addComment(@RequestBody CreateUpdateCommentDTO newComment) throws DataInvalid, NotFound {
        return commentService.addComment(newComment);
    }

    @PutMapping("/{commentId}")
    private CommentDTO updateComment(@PathVariable UUID commentId, @RequestBody CreateUpdateCommentDTO newCommentData) throws DataInvalid, NotFound {
        return commentService.updateComment(commentId, newCommentData);
    }

    @DeleteMapping("/{commentId}")
    private CommentDTO deleteCommentById(@PathVariable UUID commentId) throws NotFound {
        return commentService.deleteCommentById(commentId);
    }
}
