package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.UserService;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateUserDTO;
import com.ewakom.spring.leftovers.service.dto.UserDTO;
import com.ewakom.spring.leftovers.service.exception.AlreadyExists;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    private List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{userId}")
    private UserDTO getUserById(@PathVariable UUID userId) throws NotFound {
        return userService.getUserById(userId);
    }

    @GetMapping("/login/{userLogin}")
    private UserDTO getUserByLogin(@PathVariable String userLogin) throws NotFound {
        return userService.getUserByLogin(userLogin);
    }

    @PostMapping
    private UserDTO addUser(@RequestBody CreateUpdateUserDTO newUser) throws AlreadyExists, DataInvalid {
        return userService.addUser(newUser);
    }

    @PutMapping("/{userId}")
    private UserDTO updateUser(@PathVariable UUID userId, @RequestBody CreateUpdateUserDTO newUserData) throws NotFound, DataInvalid {
        return userService.updateUser(userId, newUserData);
    }

    @DeleteMapping("/{userId}")
    private UserDTO deleteUserById(@PathVariable UUID userId) throws NotFound {
        return userService.deleteUserById(userId);
    }

    @DeleteMapping("/login/{userLogin}")
    private UserDTO deleteUserByLogin(@PathVariable String userLogin) throws NotFound {
        return userService.deleteUserByLogin(userLogin);
    }
}
