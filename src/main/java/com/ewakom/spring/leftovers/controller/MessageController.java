package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.MessageService;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateMessageDTO;
import com.ewakom.spring.leftovers.service.dto.MessageDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/messages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping
    private List<MessageDTO> getAllMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping("/{messageId}")
    private MessageDTO getMessageById(@PathVariable UUID messageId) throws NotFound {
        return messageService.getMessageById(messageId);
    }

    @PostMapping
    private MessageDTO addMessage(@RequestBody CreateUpdateMessageDTO newMessage) throws DataInvalid, NotFound {
        return messageService.addMessage(newMessage);
    }

    @PutMapping("/{messageId}")
    private MessageDTO updateMessage(@PathVariable UUID messageId, @RequestBody CreateUpdateMessageDTO newMessageData) throws DataInvalid, NotFound {
        return messageService.updateMessage(messageId, newMessageData);
    }

    @DeleteMapping("/{messageId}")
    private MessageDTO deleteMessageById(@PathVariable UUID messageId) throws NotFound {
        return messageService.deleteMessageById(messageId);
    }
}
