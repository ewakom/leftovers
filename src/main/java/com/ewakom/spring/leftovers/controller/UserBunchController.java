package com.ewakom.spring.leftovers.controller;

import com.ewakom.spring.leftovers.service.UserBunchService;
import com.ewakom.spring.leftovers.service.dto.BunchDTO;
import com.ewakom.spring.leftovers.service.dto.UserDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class UserBunchController {
    @Autowired
    private UserBunchService userBunchService;

    @GetMapping("/users/{userId}/bunches")
    private List<BunchDTO> getAllBunchesForUser(@PathVariable UUID userId) throws NotFound{
        return userBunchService.getAllBunchesForUser(userId);
    }

    @PostMapping("/users/{userId}/bunches/{bunchId}")
    private UserDTO addBunchToUser(@PathVariable UUID userId, @PathVariable UUID bunchId) throws NotFound {
        return userBunchService.addBunchToUser(userId, bunchId);
    }

    @DeleteMapping("/users/{userId}/bunches/{bunchId}")
    private UserDTO deleteBunchFromUser(@PathVariable UUID userId, @PathVariable UUID bunchId) throws NotFound {
        return userBunchService.deleteBunchFromUser(userId, bunchId);
    }
}
