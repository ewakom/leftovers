package com.ewakom.spring.leftovers.repository;

import com.ewakom.spring.leftovers.model.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface NoticeRepository extends JpaRepository<Notice, UUID> {
}
