package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class NoticeDTO {
    private UUID id;
    private String body;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;
    private UUID authorId;
    private List<UUID> products = new ArrayList<>();

    @Override
    public String toString() {
        return "NoticeDTO{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", authorId=" + authorId +
                '}';
    }
}
