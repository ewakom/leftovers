package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.model.Product;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.repository.UserRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateNoticeDTO;
import com.ewakom.spring.leftovers.service.dto.NoticeDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class NoticeMapper {
    @Autowired
    private UserRepository userRepository;

    public NoticeDTO mapToDTO(Notice notice) {
        UUID authorId = notice.getAuthor() != null ? notice.getAuthor().getId() : null;
        List<UUID> productsId = notice.getProducts().stream()
                .map(Product::getId)
                .collect(Collectors.toList());
        return new NoticeDTO(notice.getId(), notice.getBody(), notice.getCreatedAt(), notice.getUpdatedAt(), authorId,productsId);
    }

    public Notice mapToModel(CreateUpdateNoticeDTO newNotice) throws NotFound {
        UUID authorId = newNotice.getAuthorId();
        User author = userRepository.findById(authorId).orElseThrow(() -> new NotFound("User with ID " + authorId.toString() + " does not exist."));
        return new Notice(UUID.randomUUID(), newNotice.getBody(), OffsetDateTime.now(), null
                , author, new ArrayList<>(), new ArrayList<>());
    }

}
