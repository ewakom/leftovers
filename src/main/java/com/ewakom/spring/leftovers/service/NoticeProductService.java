package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.model.Product;
import com.ewakom.spring.leftovers.repository.NoticeRepository;
import com.ewakom.spring.leftovers.repository.ProductRepository;
import com.ewakom.spring.leftovers.service.dto.NoticeDTO;
import com.ewakom.spring.leftovers.service.dto.ProductDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.NoticeMapper;
import com.ewakom.spring.leftovers.service.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class NoticeProductService {
    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private NoticeMapper noticeMapper;
    @Autowired
    private ProductMapper productMapper;

    @Transactional
    public List<ProductDTO> getAllProductsForNotice(UUID noticeId) throws NotFound {
        Notice notice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new NotFound("Notice with ID" + noticeId.toString() + "does not exist."));
        return notice.getProducts().stream().map(product -> productMapper.mapToDTO(product)).collect(Collectors.toList());
    }
    @Transactional
    public NoticeDTO addProductToNotice(UUID noticeId, UUID productId) throws NotFound {
        Notice notice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new NotFound("Notice with ID" + noticeId.toString() + "does not exist."));
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFound("Product with ID" + productId.toString() + "does not exist."));
        notice.getProducts().add(product);
        product.getNotices().add(notice);
        return noticeMapper.mapToDTO(notice);
    }
    @Transactional
    public NoticeDTO deleteProductFromNotice(UUID noticeId, UUID productId) throws NotFound {
        Notice notice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new NotFound("Notice with ID" + noticeId.toString() + "does not exist."));
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFound("Product with ID" + productId.toString() + "does not exist."));
        notice.getProducts().remove(product);
        product.getNotices().remove(notice);
        return noticeMapper.mapToDTO(notice);
    }
}
