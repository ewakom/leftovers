package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Message;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.repository.UserRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateMessageDTO;
import com.ewakom.spring.leftovers.service.dto.MessageDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.UUID;

@Component
public class MessageMapper {
    @Autowired
    private UserRepository userRepository;

    public MessageDTO mapToDTO(Message message) {
        UUID authorId = message.getAuthor() != null ? message.getAuthor().getId() : null;
        UUID receiverId = message.getReceiver() != null ? message.getReceiver().getId() : null;
        return new MessageDTO(message.getId(), message.getBody(), message.getCreatedAt(), message.getReadAt(), authorId, receiverId);
    }

    public Message mapToModel(CreateUpdateMessageDTO newMessage) throws NotFound {
        User author = userRepository.findById(newMessage.getAuthorId()).orElseThrow(NotFound::new);
        User receiver = userRepository.findById(newMessage.getReceiverId()).orElseThrow(NotFound::new);
        return new Message(UUID.randomUUID(), newMessage.getBody(), OffsetDateTime.now(), null, author, receiver);
    }

}
