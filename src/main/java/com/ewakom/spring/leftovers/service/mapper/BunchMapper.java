package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Bunch;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.service.dto.BunchDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateBunchDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class BunchMapper {

    public BunchDTO mapToDTO(Bunch bunch){
        List<UUID> usersId = bunch.getUsers().stream()
                .map(User::getId)
                .collect(Collectors.toList());
        return new BunchDTO(bunch.getId(),bunch.getName(),usersId);
    }
    public Bunch mapToModel(CreateUpdateBunchDTO newBunch){
        return new Bunch(UUID.randomUUID(),newBunch.getName(),new ArrayList<>());
    }
}
