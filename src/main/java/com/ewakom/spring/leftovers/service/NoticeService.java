package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.repository.NoticeRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateNoticeDTO;
import com.ewakom.spring.leftovers.service.dto.NoticeDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.NoticeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class NoticeService {
    @Autowired
    private NoticeRepository noticeRepository;
    @Autowired
    private NoticeMapper mapper;

    @Transactional
    public List<NoticeDTO> getAllNotices() {
        return noticeRepository.findAll().stream()
                .map(notice -> mapper.mapToDTO(notice))
                .collect(Collectors.toList());
    }

    @Transactional
    public NoticeDTO getNoticeById(UUID noticeId) throws NotFound {
        return noticeRepository.findById(noticeId)
                .map(notice -> mapper.mapToDTO(notice))
                .orElseThrow(() -> new NotFound("Notice with ID " + noticeId.toString() + " does not exist."));
    }

    @Transactional
    public NoticeDTO addNotice(CreateUpdateNoticeDTO newNotice) throws DataInvalid, NotFound {
        validateData(newNotice);
        Notice notice = mapper.mapToModel(newNotice);
        Notice savedNotice = noticeRepository.save(notice);
        return mapper.mapToDTO(savedNotice);
    }

    @Transactional
    public NoticeDTO updateNotice(UUID noticeId, CreateUpdateNoticeDTO newNoticeData) throws DataInvalid, NotFound {
        validateData(newNoticeData);
        Notice notice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new NotFound("Notice with ID " + noticeId.toString() + " does not exist."));
        notice.setBody(newNoticeData.getBody());
        notice.setUpdatedAt(OffsetDateTime.now());
        Notice savedNotice = noticeRepository.save(notice);
        return mapper.mapToDTO(savedNotice);
    }

    @Transactional
    public NoticeDTO deleteNoticeById(UUID noticeId) throws NotFound {
        Notice notice = noticeRepository.findById(noticeId)
                .orElseThrow(() -> new NotFound("Notice with ID " + noticeId.toString() + " does not exist."));
        noticeRepository.delete(notice);
        return mapper.mapToDTO(notice);
    }


    private void validateData(CreateUpdateNoticeDTO newNotice) throws DataInvalid {
        if (newNotice.getBody() == null || newNotice.getBody().length() < 3) {
            throw new DataInvalid("Complete note's body! At least 2 sign!");
        }
    }
}
