package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateNoticeDTO {
    private String body;
    private UUID authorId;

    @Override
    public String toString() {
        return "CreateUpdateNoticeDTO{" +
                "body='" + body + '\'' +
                ", authorId=" + authorId +
                '}';
    }
}
