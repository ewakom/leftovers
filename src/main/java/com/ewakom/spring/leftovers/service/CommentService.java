package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Comment;
import com.ewakom.spring.leftovers.repository.CommentRepository;
import com.ewakom.spring.leftovers.service.dto.CommentDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateCommentDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentMapper mapper;

    @Transactional
    public List<CommentDTO> getAllComments() {
        return commentRepository.findAll().stream()
                .map(comment -> mapper.mapToDTO(comment))
                .collect(Collectors.toList());
    }

    @Transactional
    public CommentDTO getCommentById(UUID commentId) throws NotFound {
        return commentRepository.findById(commentId)
                .map(comment -> mapper.mapToDTO(comment))
                .orElseThrow(() -> new NotFound("Comment with ID " + commentId.toString() + " does not exist."));
    }

    @Transactional
    public CommentDTO addComment(CreateUpdateCommentDTO newComment) throws DataInvalid, NotFound {
        validateData(newComment);
        Comment comment = mapper.mapToModel(newComment);
        Comment savedComment = commentRepository.save(comment);
        return mapper.mapToDTO(savedComment);
    }

    @Transactional
    public CommentDTO updateComment(UUID commentId, CreateUpdateCommentDTO newCommentData) throws DataInvalid, NotFound {
        validateData(newCommentData);
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new NotFound("Comment with ID " + commentId.toString() + " does not exist."));
        comment.setBody(newCommentData.getBody());
        comment.setUpdatedAt(OffsetDateTime.now());
        Comment savedComment = commentRepository.save(comment);
        return mapper.mapToDTO(savedComment);
    }

    @Transactional
    public CommentDTO deleteCommentById(UUID commentId) throws NotFound {
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new NotFound("Comment with ID " + commentId.toString() + " does not exist."));
        commentRepository.delete(comment);
        return mapper.mapToDTO(comment);
    }

    private void validateData(CreateUpdateCommentDTO newComment) throws DataInvalid {
        if (newComment.getBody() == null || newComment.getBody().length() < 3) {
            throw new DataInvalid("Complete comment's body! At least 2 sign!");
        }
    }
}
