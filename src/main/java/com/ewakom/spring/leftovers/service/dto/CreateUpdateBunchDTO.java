package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateBunchDTO {
    private String name;

    @Override
    public String toString() {
        return "CreateUpdateBunchDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
