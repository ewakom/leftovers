package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Product;
import com.ewakom.spring.leftovers.repository.ProductRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateProductDTO;
import com.ewakom.spring.leftovers.service.dto.ProductDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper mapper;

    @Transactional
    public List<ProductDTO> getAllProducts() {
        return productRepository.findAll().stream()
                .map(product -> mapper.mapToDTO(product))
                .collect(Collectors.toList());
    }

    @Transactional
    public ProductDTO getProductById(UUID productId) throws NotFound {
        return productRepository.findById(productId).map(product -> mapper.mapToDTO(product))
                .orElseThrow(() -> new NotFound("Product with ID" + productId.toString() + " does not exist."));
    }

    @Transactional
    public ProductDTO addProduct(CreateUpdateProductDTO newProduct) throws DataInvalid {
        validateDate(newProduct);
        Product product = mapper.mapToModel(newProduct);
        Product savedProduct = productRepository.save(product);
        return mapper.mapToDTO(savedProduct);
    }

    @Transactional
    public ProductDTO updateProduct(UUID productId, CreateUpdateProductDTO newProductDate) throws DataInvalid, NotFound {
        validateDate(newProductDate);
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFound("Product with ID" + productId.toString() + " does not exist."));
        product.setName(newProductDate.getName());
        Product savedProduct = productRepository.save(product);
        return mapper.mapToDTO(savedProduct);
    }

    @Transactional
    public ProductDTO deleteProductById(UUID productId) throws NotFound {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFound("Product with ID" + productId.toString() + " does not exist."));
        productRepository.delete(product);
        return mapper.mapToDTO(product);
    }

    private void validateDate(CreateUpdateProductDTO newProduct) throws DataInvalid {
        if (newProduct.getName() == null || newProduct.getName().length() < 3) {
            throw new DataInvalid("Complete product's name! At least 2 sign!");
        }
    }
}
