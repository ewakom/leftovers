package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Bunch;
import com.ewakom.spring.leftovers.repository.BunchRepository;
import com.ewakom.spring.leftovers.service.dto.BunchDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateBunchDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.BunchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BunchService {
    @Autowired
    private BunchRepository bunchRepository;
    @Autowired
    private BunchMapper mapper;

    @Transactional
    public List<BunchDTO> getAllBunches() {
        return bunchRepository.findAll().stream()
                .map(bunch -> mapper.mapToDTO(bunch))
                .collect(Collectors.toList());
    }

    @Transactional
    public BunchDTO getBunchById(UUID bunchId) throws NotFound {
        return bunchRepository.findById(bunchId).map(bunch -> mapper.mapToDTO(bunch))
                .orElseThrow(() -> new NotFound("Bunch with ID" + bunchId.toString() + " does not exist."));
    }

    @Transactional
    public BunchDTO addBunch(CreateUpdateBunchDTO newBunch) throws DataInvalid {
        validateData(newBunch);
        Bunch bunch = mapper.mapToModel(newBunch);
        Bunch savedBunch = bunchRepository.save(bunch);
        return mapper.mapToDTO(savedBunch);
    }

    @Transactional
    public BunchDTO updateBunch(UUID bunchId, CreateUpdateBunchDTO newBunchData) throws DataInvalid, NotFound {
        validateData(newBunchData);
        Bunch bunch = bunchRepository.findById(bunchId).orElseThrow(NotFound::new);
        bunch.setName(newBunchData.getName());
        Bunch savedBunch = bunchRepository.save(bunch);
        return mapper.mapToDTO(savedBunch);
    }

    @Transactional
    public BunchDTO deleteBunchById(UUID bunchId) throws NotFound {
        Bunch bunch = bunchRepository.findById(bunchId).orElseThrow(NotFound::new);
        bunchRepository.delete(bunch);
        return mapper.mapToDTO(bunch);
    }

    private void validateData(CreateUpdateBunchDTO newBunch) throws DataInvalid {
        if (newBunch.getName() == null || newBunch.getName().length() < 3) {
            throw new DataInvalid("Complete bunch's name! At least 2 sign!");
        }
    }
}
