package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateMessageDTO {
    private String body;
    private UUID authorId;
    private UUID receiverId;

    @Override
    public String toString() {
        return "CreateUpdateMessageDTO{" +
                "body='" + body + '\'' +
                ", authorId=" + authorId +
                ", receiverId=" + receiverId +
                '}';
    }
}
