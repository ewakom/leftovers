package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.repository.UserRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateUserDTO;
import com.ewakom.spring.leftovers.service.dto.UserDTO;
import com.ewakom.spring.leftovers.service.exception.AlreadyExists;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public List<UserDTO> getAllUsers() {
        return userRepository.findAll().stream()
                .map(user -> userMapper.mapToDTO(user))
                .collect(Collectors.toList());
    }

    @Transactional
    public UserDTO getUserById(UUID userId) throws NotFound {
        return userRepository.findById(userId)
                .map(user -> userMapper.mapToDTO(user))
                .orElseThrow(() -> new NotFound("User with ID " + userId.toString() + " does not exist."));
    }

    @Transactional
    public UserDTO getUserByLogin(String userLogin) throws NotFound {
        return userRepository.findByLogin(userLogin)
                .map(user -> userMapper.mapToDTO(user))
                .orElseThrow(() -> new NotFound("User with Login " + userLogin + " does not exist."));
    }

    @Transactional
    public UserDTO addUser(CreateUpdateUserDTO newUser) throws DataInvalid, AlreadyExists {
        validateData(newUser);
        if (userRepository.existsByLogin(newUser.getLogin())) {
            throw new AlreadyExists("User with Login " + newUser.getLogin() + " already exist.");
        }
        User user = userMapper.mapToModel(newUser);
        User savedUser = userRepository.save(user);
        return userMapper.mapToDTO(savedUser);
    }

    @Transactional
    public UserDTO updateUser(UUID userId, CreateUpdateUserDTO newUserData) throws DataInvalid, NotFound {
        validateData(newUserData);
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFound("User with ID " + userId.toString() + " does not exist."));
        user.setLogin(newUserData.getLogin());
        user.setPassword(newUserData.getPassword());
        user.setName(newUserData.getName());
        user.setSurname(newUserData.getSurname());
        user.setAge(newUserData.getAge());
        User savedUser = userRepository.save(user);
        return userMapper.mapToDTO(savedUser);
    }

    @Transactional
    public UserDTO deleteUserById(UUID userId) throws NotFound {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFound("User with ID " + userId.toString() + " does not exist."));
        userRepository.delete(user);
        return userMapper.mapToDTO(user);
    }

    @Transactional
    public UserDTO deleteUserByLogin(String userLogin) throws NotFound {
        User user = userRepository.findByLogin(userLogin).orElseThrow(() -> new NotFound("User with Login " + userLogin + " does not exist."));
        userRepository.delete(user);
        return userMapper.mapToDTO(user);
    }

    private void validateData(CreateUpdateUserDTO newUser) throws DataInvalid {
        if (newUser.getPassword() == null || newUser.getPassword().length() < 3) {
            throw new DataInvalid("Incorrect password. Must be longer than 2 signs!");
        }
    }
}
