package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Message;
import com.ewakom.spring.leftovers.repository.MessageRepository;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateMessageDTO;
import com.ewakom.spring.leftovers.service.dto.MessageDTO;
import com.ewakom.spring.leftovers.service.exception.DataInvalid;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageMapper mapper;

    @Transactional
    public List<MessageDTO> getAllMessages() {
        return messageRepository.findAll().stream()
                .map(message -> mapper.mapToDTO(message))
                .collect(Collectors.toList());
    }

    @Transactional
    public MessageDTO getMessageById(UUID messageId) throws NotFound {
        return messageRepository.findById(messageId).map(message -> mapper.mapToDTO(message))
                .orElseThrow(() -> new NotFound("Message with ID" + messageId.toString() + " does not exist."));
    }

    @Transactional
    public MessageDTO addMessage(CreateUpdateMessageDTO newMessage) throws DataInvalid, NotFound {
        validateData(newMessage);
        Message message = mapper.mapToModel(newMessage);
        Message savedMessage = messageRepository.save(message);
        return mapper.mapToDTO(savedMessage);
    }

    @Transactional
    public MessageDTO updateMessage(UUID messageId, CreateUpdateMessageDTO newMessageData) throws DataInvalid, NotFound {
        validateData(newMessageData);
        Message message = messageRepository.findById(messageId)
                .orElseThrow(() -> new NotFound("Message with ID" + messageId.toString() + " does not exist."));
        message.setBody(newMessageData.getBody());
        Message savedMessage = messageRepository.save(message);
        return mapper.mapToDTO(savedMessage);
    }

    @Transactional
    public MessageDTO deleteMessageById(UUID messageId) throws NotFound {
        Message message = messageRepository.findById(messageId)
                .orElseThrow(() -> new NotFound("Message with ID" + messageId.toString() + " does not exist."));
        messageRepository.delete(message);
        return mapper.mapToDTO(message);
    }

    private void validateData(CreateUpdateMessageDTO newMessage) throws DataInvalid {
        if (newMessage.getBody() == null || newMessage.getBody().length() < 3) {
            throw new DataInvalid("Complete message's body! At least 2 sign!");
        }
    }
}
