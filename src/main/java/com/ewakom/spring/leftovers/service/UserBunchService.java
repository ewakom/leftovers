package com.ewakom.spring.leftovers.service;

import com.ewakom.spring.leftovers.model.Bunch;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.repository.BunchRepository;
import com.ewakom.spring.leftovers.repository.UserRepository;
import com.ewakom.spring.leftovers.service.dto.BunchDTO;
import com.ewakom.spring.leftovers.service.dto.UserDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import com.ewakom.spring.leftovers.service.mapper.BunchMapper;
import com.ewakom.spring.leftovers.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserBunchService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BunchRepository bunchRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private BunchMapper bunchMapper;

    @Transactional
    public List<BunchDTO> getAllBunchesForUser(UUID userId) throws NotFound {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFound("User with ID" + userId.toString() + "does not exist."));
        return user.getBunches().stream().map(bunch -> bunchMapper.mapToDTO(bunch)).collect(Collectors.toList());
    }

    @Transactional
    public UserDTO addBunchToUser(UUID userId, UUID bunchId) throws NotFound {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFound("User with ID" + userId.toString() + "does not exist."));
        Bunch bunch = bunchRepository.findById(bunchId)
                .orElseThrow(() -> new NotFound("Bunch with ID" + bunchId.toString() + "does not exist."));
        user.getBunches().add(bunch);
        bunch.getUsers().add(user);
        return userMapper.mapToDTO(user);
    }

    @Transactional
    public UserDTO deleteBunchFromUser(UUID userId, UUID bunchId) throws NotFound {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFound("User with ID" + userId.toString() + "does not exist."));
        Bunch bunch = bunchRepository.findById(bunchId)
                .orElseThrow(() -> new NotFound("Bunch with ID" + bunchId.toString() + "does not exist."));
        user.getBunches().remove(bunch);
        bunch.getUsers().remove(user);
        return userMapper.mapToDTO(user);
    }
}
