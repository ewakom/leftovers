package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Bunch;
import com.ewakom.spring.leftovers.model.Message;
import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateUserDTO;
import com.ewakom.spring.leftovers.service.dto.UserDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    public UserDTO mapToDTO(User user) {
        List<UUID> bunchesId = user.getBunches().stream()
                .map(Bunch::getId)
                .collect(Collectors.toList());
        List<UUID> noticesId = user.getNotices().stream()
                .map(Notice::getId)
                .collect(Collectors.toList());
        List<UUID> sendMessagesId = user.getSendMessages().stream()
                .map(Message::getId)
                .collect(Collectors.toList());
        List<UUID> receivedMessagesId = user.getReceivedMessages().stream()
                .map(Message::getId)
                .collect(Collectors.toList());
        return new UserDTO(user.getId(), user.getLogin(), user.getName(), user.getSurname()
                , user.getAge(), bunchesId, noticesId, sendMessagesId, receivedMessagesId);
    }

    public User mapToModel(CreateUpdateUserDTO newUser) {
        return new User(UUID.randomUUID(), newUser.getLogin(), newUser.getPassword(), newUser.getName()
                , newUser.getSurname(), newUser.getAge(), new ArrayList<>(), new ArrayList<>()
                , new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }
}
