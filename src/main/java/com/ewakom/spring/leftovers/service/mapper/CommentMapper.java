package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Comment;
import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.model.User;
import com.ewakom.spring.leftovers.repository.NoticeRepository;
import com.ewakom.spring.leftovers.repository.UserRepository;
import com.ewakom.spring.leftovers.service.dto.CommentDTO;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateCommentDTO;
import com.ewakom.spring.leftovers.service.exception.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.UUID;

@Component
public class CommentMapper {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NoticeRepository noticeRepository;

    public CommentDTO mapToDTO (Comment comment){
        UUID authorId = comment.getAuthor() != null ? comment.getAuthor().getId() : null;
        UUID noticeId = comment.getNotice() != null ? comment.getNotice().getId() : null;
        return new CommentDTO(comment.getId(), comment.getBody(),noticeId, authorId, comment.getCreatedAt(), comment.getUpdatedAt());
    }

    public Comment mapToModel (CreateUpdateCommentDTO newComment) throws NotFound {
        User author = userRepository.findById(newComment.getAuthorId()).orElseThrow(NotFound::new);
        Notice notice = noticeRepository.findById(newComment.getNoticeId()).orElseThrow(NotFound::new);
        return new Comment(UUID.randomUUID(),author,notice,newComment.getBody(), OffsetDateTime.now(),null);
    }
}
