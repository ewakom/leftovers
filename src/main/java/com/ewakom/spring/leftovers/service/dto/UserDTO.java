package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class UserDTO {
    private UUID id;
    private String login;
    private String name;
    private String surname;
    private Integer age;
    private List<UUID> bunchesId = new ArrayList<>();
    private List<UUID> noticesId = new ArrayList<>();
    private List<UUID> sendMessagesId = new ArrayList<>();
    private List<UUID> receivedMessagesId = new ArrayList<>();

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
