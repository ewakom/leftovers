package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class MessageDTO {
    private UUID id;
    private String body;
    private OffsetDateTime createdAt;
    private OffsetDateTime readAt;
    private UUID authorId;
    private UUID receiverId;

    @Override
    public String toString() {
        return "MessageDTO{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                ", readAt=" + readAt +
                ", authorId=" + authorId +
                ", receiverId=" + receiverId +
                '}';
    }
}
