package com.ewakom.spring.leftovers.service.mapper;

import com.ewakom.spring.leftovers.model.Notice;
import com.ewakom.spring.leftovers.model.Product;
import com.ewakom.spring.leftovers.service.dto.CreateUpdateProductDTO;
import com.ewakom.spring.leftovers.service.dto.ProductDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    public ProductDTO mapToDTO(Product product) {
        List<UUID> noticesId = product.getNotices().stream()
                .map(Notice::getId)
                .collect(Collectors.toList());
        return new ProductDTO(product.getId(), product.getName(), noticesId);
    }

    public Product mapToModel(CreateUpdateProductDTO newProduct) {
        return new Product(UUID.randomUUID(), newProduct.getName(), new ArrayList<>());
    }
}
