package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class BunchDTO {
    private UUID id;
    private String name;
    private List<UUID> usersId = new ArrayList<>();

    @Override
    public String toString() {
        return "BunchDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", usersId=" + usersId +
                '}';
    }
}
