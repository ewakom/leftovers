package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateCommentDTO {
    private String body;
    private UUID noticeId;
    private UUID authorId;

    @Override
    public String toString() {
        return "CreateUpdateCommentDTO{" +
                "body='" + body + '\'' +
                ", noticeId=" + noticeId +
                ", authorId=" + authorId +
                '}';
    }
}
