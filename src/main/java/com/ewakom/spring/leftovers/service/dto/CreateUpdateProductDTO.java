package com.ewakom.spring.leftovers.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateProductDTO {

    private String name;

    @Override
    public String toString() {
        return "CreateUpdateProductDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
