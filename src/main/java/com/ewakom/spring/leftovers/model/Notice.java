package com.ewakom.spring.leftovers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class Notice {
    @Id
    private UUID id;
    private String body;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "author_id")
    private User author;
    @ManyToMany(mappedBy = "notices", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Product> products;
    @OneToMany(mappedBy = "notice", cascade = {CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})
    private List<Comment> comments = new ArrayList<>();

    @Override
    public String toString() {
        return "Notice{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", authorId=" + author.getId() +
                '}';
    }
}
