package com.ewakom.spring.leftovers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
public class Bunch {
    @Id
    private UUID id;
    private String name;
    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private List<User> users = new ArrayList<>();

    @Override
    public String toString() {
        return "Bunch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", usersId=" + users.stream().map(User::getId) +
                '}';
    }
}
