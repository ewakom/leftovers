### "Leftovers" - SPRING Web Application (in progress)
The idea - you have food that has a short expiration date or you cooked too much - Share it! Do not throw!

### About this application:
- users belong to specific groups, micro groups of users who know and trust each other.
 Example of a group - neighbors from the same floor.
- as a user of the group, you can give posts about food / food products that you want to share.
- each notice can be transcribed to a specific group of products, e.g. short expiry date / I cooked / dairy / vegetables, etc.
- comments can be added to each notice
- users can send messages

###
The application development environment:
- IntelliJ IDEA
- Maven dependencies:
spring-boot-starter-data-jpa; spring-boot-starter-web; spring-boot-starter-test; 
swagger; thymeleaf; lombok; PostrgreSQL; 
